package com.testing.common.service;

import com.testing.common.entity.Question;
import com.testing.common.entity.Answer;



import com.testing.common.manager.remote.TestingPersistingBeanRemote;
import com.testing.common.manager.TestingPersistingBean;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ejb.EJB;
import java.util.List;

/**
 * Created by admin on 24.08.2015.
 */
public class TestingService {


	public static int QUESTIONS_NUMBER = 2;
	
    public List<Question> getQuestions() {
    	InitialContext ini;
    	try {
			ini = new InitialContext();
			TestingPersistingBeanRemote testBean = (TestingPersistingBeanRemote) ini.lookup("java:module/TestingPersistingBean");
			return testBean.getQuestions();
		} catch (NamingException e) {
			e.printStackTrace();
		}
    	return null;
    }
    
    
    public float getMark(List<Integer> answers) {
    	InitialContext ini;
    	int countRight = 0;
    	try {
			ini = new InitialContext();
			TestingPersistingBeanRemote testBean = (TestingPersistingBeanRemote) ini.lookup("java:module/TestingPersistingBean");
			for(Integer id : answers){
				Answer answer = testBean.getAnswerById(id);
				if (answer.isRight()){
					countRight++;
				}
			}
		} catch (NamingException e) {
			e.printStackTrace();
		}
    	return ((float) countRight/QUESTIONS_NUMBER) * 100;
    }
}
