package com.testing.common.servlet;

import com.testing.common.manager.remote.TestingPersistingBeanRemote;
import com.testing.common.entity.Question;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class TestingServlet extends HttpServlet implements Servlet {

    @EJB
    TestingPersistingBeanRemote testingPersisting;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            List<Question> questions = testingPersisting.getQuestions();
            req.getSession().setAttribute("questions", questions);
            RequestDispatcher rd = req.getRequestDispatcher("testingPage.jsp");
            rd.forward(req, resp);
    }
}
