package com.testing.common.manager.remote;

import com.testing.common.entity.Answer;
import com.testing.common.entity.Question;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface TestingPersistingBeanRemote {
    List<Answer> getAnswers();
    List<Question> getQuestions();
    Answer getAnswerById(Integer id);
    void addAnswer(Answer answer);
}
