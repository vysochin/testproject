package testing.common.service;

import testing.common.entity.Answer;
import testing.common.entity.Question;
import testing.common.manager.TestingPersistingBean;

import javax.ejb.EJB;
import java.util.List;

/**
 * Created by admin on 24.08.2015.
 */
public class TestingService {


	public static int QUESTIONS_NUMBER = 2;

	@EJB
	TestingPersistingBean testBean;

	
    public List<Question> getQuestions() {
			return testBean.getQuestions();
    }
    
    
    public float getMark(List<Integer> answers) {
    	int countRight = 0;

			for(Integer id : answers){
				Answer answer = testBean.getAnswerById(id);
				if (answer.isRight()){
					countRight++;
				}
			}

    	return ((float) countRight/QUESTIONS_NUMBER) * 100;
    }

	public String getTestString() {
		return "Go go go";
	}
}
