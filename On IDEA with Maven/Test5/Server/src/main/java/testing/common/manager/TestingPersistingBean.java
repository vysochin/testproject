package testing.common.manager;

import testing.common.entity.Answer;
import testing.common.entity.Question;
import testing.common.manager.remote.TestingPersistingBeanRemote;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class TestingPersistingBean implements TestingPersistingBeanRemote {

    public TestingPersistingBean (){}

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Answer> getAnswers() {
        return  entityManager.createQuery("From Answer").getResultList();
    }

    @Override
    public List<Question> getQuestions() {
        return  entityManager.createQuery("From Question").getResultList();
    }

    @Override
    public void addAnswer(Answer answer) {
        entityManager.persist(answer);
    }
    
    @Override
    public Answer getAnswerById (Integer id){
    	return (Answer) entityManager.createQuery("From Answer where ID = " + id.intValue()).getSingleResult();
    }
}
