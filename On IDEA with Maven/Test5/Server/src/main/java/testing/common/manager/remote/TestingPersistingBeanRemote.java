package testing.common.manager.remote;

import testing.common.entity.Answer;
import testing.common.entity.Question;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface TestingPersistingBeanRemote {
    List<Answer> getAnswers();
    List<Question> getQuestions();
    Answer getAnswerById(Integer id);
    void addAnswer(Answer answer);
}
